import sys
import argparse
from SpiderWeb import SpiderWeb

def main():
    parser = argparse.ArgumentParser(description='Social media scraping tool')
    parser.add_argument('-l', '--limit', type=int, default='10', help='Maximum entries to retrieve')
    parser.add_argument('-o', '--outputdir', type=str, default='.', help='Output directory')

    subparsers = parser.add_subparsers(help='Types of social media websites')
    ig_parser = subparsers.add_parser("instagram")
    ig_parser.set_defaults(which='ig')
    ig_parser.add_argument('-t', '--tags', type=str, help='Hashtags to process. ', nargs='+', default=[])
    ig_parser.add_argument('-u', '--users', type=str, help='Hashtags to process. ', nargs='+', default=[])

    fb_parser = subparsers.add_parser("facebook")
    fb_parser.set_defaults(which='fb')
    # TO DO

    args = parser.parse_args()

    if not len(sys.argv) > 1:
        parser.print_help()
        sys.exit()

    targets = ['tags/'+x for x in args.tags] + ['user/'+x for x in args.users]

    web = SpiderWeb(args.which, outputdir=args.outputdir)
    web.run(targets=targets, limit=args.limit)

if __name__ == "__main__":
    main()