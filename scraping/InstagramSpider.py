import json
import scrapy
from scrapy import Spider
import json
import os
import urllib
import datetime


class InstagramSpider(Spider):
    '''MSS: Instagram Spider'''

    def __init__(self, target, limit, outputdir):

        self.name = 'Instagram'
        self.start_urls = [
            'https://www.instagram.com/'
        ]
        self.limit = limit
        self.account = ''
        self.target = target

        if 'tags' in target:
            self.user_type = 'tag'
            self.account = 'explore/'+target
        if 'user' in target:
            self.user_type = 'user'
            self.account = target.replace('user/', '')

        self.start_urls[0] = 'https://www.instagram.com/'+self.account

        self.count = 0
        self.outputdir = outputdir
        self.output = []




    def parse(self, response):
        request = scrapy.Request(response.url, callback=self.parse_page)
        return request

    # Method for parsing a page
    def parse_page(self, response):



        #We get the json containing the photos's path
        js = response.selector.xpath('//script[contains(., "window._sharedData")]/text()').extract()
        js = js[0].replace("window._sharedData = ", "")
        jscleaned = js[:-1]

        #Load it as a json object
        locations = json.loads(jscleaned)
        #We check if there is a next page
        
        if self.user_type == 'user':
            user = locations['entry_data']['ProfilePage'][0]['user']
        elif self.user_type == 'tag':      
            user = locations['entry_data']['TagPage'][0]['tag']
        
        has_next = user['media']['page_info']['has_next_page']
        end_cursor = user['media']['page_info']['end_cursor'] # tags
        media = user['media']['nodes']

        for photo in media:
            url = photo['display_src']
            id = photo['id']
            is_video =  photo['is_video']
            comments = photo['comments']['count']
            likes = photo['likes']['count']
            date = photo['date']
            
            if is_video:
                continue

            if self.count < self.limit:
                self.count += 1
                out = {'comments': comments, 'likes': likes, 'photo_url': url, 'id': id, 'date': date}
                self.output.append(out)
                yield out
            else:
                with open(self.outputdir+'/'+self.target.replace('/', '_')+'.json', 'w') as f:
                    json.dump(self.output, f)
                return

        if has_next:
            if self.user_type == 'user':
                url="https://www.instagram.com/"+self.account+"/?max_id="+media[-1]['id']
            elif self.user_type == 'tag':
                url="https://www.instagram.com/"+self.account+"/?max_id="+end_cursor
            
            yield scrapy.Request(url, callback=self.parse_page)