import logging
from InstagramSpider import InstagramSpider
from scrapy.crawler import CrawlerProcess
from twisted.internet import reactor
from scrapy.crawler import CrawlerRunner
from scrapy.utils.log import configure_logging

configure_logging()


class SpiderWeb:

    modules = {'ig': InstagramSpider}

    def __init__(self, social, outputdir='.'):
        self.outputdir = outputdir
        self.social = social
        self.spider = self.modules.get('ig')


    def _rnd_user_agent(self):
        # TO DO: Add other user agents
        return 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)'



    def run(self,  targets, limit):

        process = CrawlerProcess({'USER_AGENT': self._rnd_user_agent()})
        for target in targets:
            process.crawl(self.spider, target=target, limit=limit, outputdir=self.outputdir)

            print("\n\nrunning ", self.social, target, 'processes, limit', limit, "\n\n")
        process.start()